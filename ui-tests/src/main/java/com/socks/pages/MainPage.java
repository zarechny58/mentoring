package com.socks.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.socks.api.entity.User;
import com.socks.component.LoginModal;
import com.socks.component.RegistrationModal;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class MainPage {

    private final SelenideElement registerLink = $x("//a[.='Register']");
    private final SelenideElement loginLink = $x("//a[.='Login']");
    public final SelenideElement loggedUserLabel = $("#howdy > a");

    RegistrationModal registerModal = new RegistrationModal();
    LoginModal loginModal = new LoginModal();

    @Step("Opening page")
    public MainPage open() {
        return Selenide.open("/", MainPage.class);
    }

    @Step("Register user {0}")
    public MainPage registerUser(User user) {
        registerLink.click();
        registerModal.fillRegistrationData(user);
        return new MainPage();
    }

    @Step("Login as {0}")
    public MainPage LoginAs(User user) {
        loginLink.click();
        loginModal.fillData(user);
        return new MainPage();
    }
}
