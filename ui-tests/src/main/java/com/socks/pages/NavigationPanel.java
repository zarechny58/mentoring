package com.socks.pages;

import com.codeborne.selenide.SelenideElement;
import com.socks.component.Product;

import static com.codeborne.selenide.Selenide.$;

public class NavigationPanel {

    SelenideElement catalogue = $("#tabCatalogue > a");

    public CataloguePage openCatalogue() {
        catalogue.click();
        return new CataloguePage();
    }


}
