package com.socks.component;

import com.codeborne.selenide.SelenideElement;
import com.socks.api.entity.User;
import com.socks.pages.MainPage;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class LoginModal {

    private final SelenideElement userName = $("#username-modal");
    private final SelenideElement password = $("#password-modal");
    private final SelenideElement loginBtn = $("#login-modal p > button");
    private final SelenideElement loginMsg = $("#login-message");


    @Step("Filling form {0}")
    public MainPage fillData(User user) {
        userName.setValue(user.getUsername());
        password.setValue(user.getPassword());
        loginBtn.click();
        loginMsg.shouldBe(visible);
        return new MainPage();
    }
}
