package com.socks.component;

import com.codeborne.selenide.SelenideElement;
import com.socks.api.entity.User;
import com.socks.pages.MainPage;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class RegistrationModal {

    private final SelenideElement userName = $("#register-username-modal");
    private final SelenideElement firstName = $("#register-first-modal");
    private final SelenideElement lastName = $("#register-last-modal");
    private final SelenideElement email = $("#register-email-modal");
    private final SelenideElement password = $("#register-password-modal");
    private final SelenideElement registerBtn = $("#register-modal p>button");
    private final SelenideElement registrationMsg = $("#registration-message");

    @Step("Filling form {0}")
    public MainPage fillRegistrationData(User user) {
        userName.setValue(user.getUsername());
        firstName.setValue(user.getFirstName());
        lastName.setValue(user.getLastName());
        email.setValue(user.getEmail());
        password.setValue(user.getPassword());
        registerBtn.click();
        registrationMsg.shouldBe(visible);
        return new MainPage();
    }

}
