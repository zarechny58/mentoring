package com.socks.component;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static java.lang.Integer.parseInt;

public class Basket {

    public SelenideElement basket = $("#basket-overview > a>span");

    public int getItemsQuantity() {

        int i = parseInt(basket.getText().substring(0, 1).trim());
        return i;
    }
}
