package com.socks.component;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Product {

    private SelenideElement element;

    public Product(String selector) {
        this.element = $(selector);
    }

    SelenideElement productCard = $("#products > div");

    public SelenideElement name() {
        return element.find("div.text > h3 > a");
    }

    private SelenideElement addToCartBtn() {
        return element.find("p.buttons >a.btn.btn-primary");
    }

    public SelenideElement quantity() {
        return element.find("#download_btn");
    }

    public void addItemToCart() {
        addToCartBtn().click();
    }

}
