package com.socks.pages.ui.tests;

import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.logevents.SelenideLogger.addListener;
import static io.restassured.RestAssured.baseURI;

public class BaseUITest {

    @BeforeSuite
    public void setUp() {
        browser = "chrome";
        baseUrl = "http://35.231.203.193";
        baseURI = "http://35.231.203.193";
        timeout = 10000;
        browserSize = "1080x768";
        //holdBrowserOpen = true;
        addListener("AllureSelenide", new AllureSelenide());
    }

    @AfterSuite
    public void tearDown() {
        close();
    }
}
