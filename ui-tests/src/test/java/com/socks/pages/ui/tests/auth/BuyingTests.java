package com.socks.pages.ui.tests.auth;

import com.socks.component.Basket;
import com.socks.pages.NavigationPanel;
import com.socks.pages.ui.tests.LoginRequiredTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class BuyingTests extends LoginRequiredTest {

    NavigationPanel panel = new NavigationPanel();
    Basket basket = new Basket();

    @Test
    public void testCanAddItemToCart() {
        panel.openCatalogue().addItemsToCart();
        assertEquals(basket.getItemsQuantity(), 1);
    }
}
