package com.socks.pages.ui.tests.auth;

import com.socks.api.entity.User;
import com.socks.pages.MainPage;
import com.socks.pages.ui.tests.LogoutRequiredTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.exactText;
import static com.socks.api.TestData.getUserForTest;
import static java.lang.String.format;

public class RegistrationTests extends LogoutRequiredTest {

    @Test
    public void testUserCanRegister() {
        User user = getUserForTest();

        new MainPage()
                .open()
                .registerUser(user)
                .loggedUserLabel.shouldHave(exactText(format
                ("Logged in as %s %s", user.getFirstName(), user.getLastName())));
    }


}
