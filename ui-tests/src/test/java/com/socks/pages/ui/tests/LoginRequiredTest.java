package com.socks.pages.ui.tests;

import com.codeborne.selenide.Selenide;
import com.socks.api.entity.User;
import com.socks.api.service.UserApiServiceAdvanced;
import com.socks.pages.MainPage;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import static com.socks.api.TestData.getUserForTest;

public class LoginRequiredTest extends BaseUITest {

    private UserApiServiceAdvanced userApiService = new UserApiServiceAdvanced();

    @BeforeClass
    public void login() {
        User user = getUserForTest();
        userApiService.registerUser(user);
        new MainPage()
                .open()
                .LoginAs(user);
    }

    @AfterClass
    public void quit() {
        Selenide.close();
    }
}
