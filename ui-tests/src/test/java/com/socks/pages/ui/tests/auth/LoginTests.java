package com.socks.pages.ui.tests.auth;

import com.socks.api.entity.User;
import com.socks.api.service.UserApiServiceAdvanced;
import com.socks.pages.MainPage;
import com.socks.pages.ui.tests.LogoutRequiredTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.exactText;
import static com.socks.api.TestData.getUserForTest;
import static java.lang.String.format;

public class LoginTests extends LogoutRequiredTest {

    private UserApiServiceAdvanced userApiService = new UserApiServiceAdvanced();

    @Test
    public void testCanLoginAsValidUser() {

        User user = getUserForTest();
        userApiService.registerUser(user);

        new MainPage()
                .open()
                .LoginAs(user)
                .loggedUserLabel.shouldHave(exactText(format
                ("Logged in as %s %s", user.getFirstName(), user.getLastName())));
    }
}
