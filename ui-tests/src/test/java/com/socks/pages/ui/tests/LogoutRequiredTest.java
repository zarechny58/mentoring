package com.socks.pages.ui.tests;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class LogoutRequiredTest extends BaseUITest {

    @BeforeClass
    public void logout() {
        Selenide.clearBrowserCookies();
    }

    @AfterClass
    public void quit() {
        Selenide.close();
    }
}
