package com.socks.api.tests.advanced;

import com.socks.api.ApiResponse;
import com.socks.api.entity.User;
import com.socks.api.service.UserApiServiceAdvanced;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static com.socks.api.TestData.getUserForTest;
import static com.socks.api.condition.Conditions.bodyField;
import static com.socks.api.condition.Conditions.statusCode;
import static io.restassured.RestAssured.baseURI;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class UserApiTests {

    private UserApiServiceAdvanced userApiService = new UserApiServiceAdvanced();

    @BeforeAll
    static void setUp() {
        baseURI = "http://35.231.203.193";
    }

    @Test
    void testCanRegisterAsValidUser() {
        //Given
/*        User user = new User()
                .setUsername("kisd1fgdfg11")
                .setPassword("kспрu1ku1")
                .setEmail("kisga11@k1uku.com")
                .setFirstName("odfgdfg1")
                .setLastName("tdfgdfg1");*/

        //When
        ApiResponse response = userApiService.registerUser(getUserForTest());

        //Then

        response.shouldHave(statusCode(200));

        response.shouldHave(bodyField("id", not(isEmptyOrNullString())));
    }

    @Test
    void testCanNotRegisterSameUserTwice() {
        //Given
/*        User user = new User()
                .setUsername("kisa11")
                .setPassword("ku1ku1")
                .setEmail("kisa1@k1uku.com")
                .setFirstName("o1")
                .setLastName("t1");*/

        //When
        userApiService.registerUser(getUserForTest());

        //Then

        ApiResponse response = userApiService.registerUser(getUserForTest());

        response.shouldHave(statusCode(500));

    }

    @Test
    void testCanGetCustomerInfo() {
        //Given
        User user = new User()
                .setUsername("kisa111")
                .setPassword("ku1ku11")
                .setEmail("kisa11@k1uku.com")
                .setFirstName("o11")
                .setLastName("t11");

        String id = userApiService.registerUser(user).getBodyField("id");
        //When

        ApiResponse response = userApiService.getCustomerInfo(id);
        //Then

        response.shouldHave(statusCode(200));

        User userById = response.as(User.class);

        assertThat(userById, equalTo(id));
    }
}
