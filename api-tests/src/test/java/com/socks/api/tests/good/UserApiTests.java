package com.socks.api.tests.good;

import com.socks.api.service.UserApiService;
import com.socks.api.entity.User;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.baseURI;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class UserApiTests {

    private UserApiService userApiService = new UserApiService();

    @BeforeAll
    static void setUp() {
        baseURI = "http://35.231.203.193";
    }

    @Test
    void testCanRegisterAsValidUser() {
        //Given
        User user = new User()
                .setUsername("dggfgddftrrdftfg")
                .setEmail("tets1@maidfgfgl.com")
                .setPassword("test12dfg45xdffg34");

        //When
        Response response = userApiService.registerUser(user);

        //Then

        assertThat(response.statusCode(), equalTo(200));

        String id = response.body().jsonPath().get("id").toString();

        assertThat(id, not(isEmptyOrNullString()));
    }

    @Test
    void testCanNotRegisterSameUserTwice() {
        //Given
        User user = new User()
                .setUsername("dfgdfg")
                .setEmail("tets1@maidfgl.com")
                .setPassword("test1245xdffg34");

        //When
        userApiService.registerUser(user);

        //Then

        Response response = userApiService.registerUser(user);

        assertThat(response.statusCode(), equalTo(500));

    }
}
