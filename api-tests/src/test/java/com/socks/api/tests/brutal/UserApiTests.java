package com.socks.api.tests.brutal;

import com.socks.api.entity.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;

public class UserApiTests {

    @BeforeAll
    static void setUp() {
        baseURI = "http://35.231.203.193";
    }

    @Test
    void testCanRegisterAsValidUser() {
        //Given
        User user = new User()
                .setUsername("dfgdfg")
                .setEmail("tets1@maidfgl.com")
                .setPassword("test1245xdffg34");

        //When
        given()
                .body(user)
                .when()
                .post("register")
                .then()
                .assertThat()
                .statusCode(200)
                .body("id", not(isEmptyOrNullString()));
        //Then
    }
}
