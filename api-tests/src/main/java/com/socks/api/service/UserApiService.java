package com.socks.api.service;

import com.socks.api.entity.User;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class UserApiService {

    private RequestSpecification given() {
        return RestAssured.given().contentType("application/json");
    }

    public Response registerUser(User user) {
        return given()
                .body(user)
                .when()
                .post("register")
                .then()
                .extract().response();
    }
}
