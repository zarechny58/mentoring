package com.socks.api.service;

import com.socks.api.ApiResponse;
import com.socks.api.entity.User;
import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.filter.Filter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.http.ContentType.JSON;
import static java.lang.Boolean.valueOf;
import static java.lang.System.getProperty;

@Slf4j
public class UserApiServiceAdvanced {

    private RequestSpecification given() {
        List<Filter> filters = new ArrayList<>();
        if (valueOf(getProperty("EnableLogging", "false"))) {

            filters.add(new RequestLoggingFilter());
            filters.add(new ResponseLoggingFilter());
        }
        filters.add(new AllureRestAssured());

        return RestAssured.given()
                .contentType(JSON)
                .filters(filters);
    }

    @Step("Registering user {0}")
    public ApiResponse registerUser(User user) {
        log.info("Registering user {}", user);

        Response response = given()
                .body(user)
                .when()
                .post("register")
                .then()
                .extract().response();
        return new ApiResponse(response);
    }

    @Step("Getting customer info, id {0}")
    public ApiResponse getCustomerInfo(String id) {
        log.info("Getting customer info {}");

        Response response = given()
                .when()
                .get("customer/{id}" , id)
                .then()
                .extract().response();
        return new ApiResponse(response);
    }

}
