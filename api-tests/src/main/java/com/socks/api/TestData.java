package com.socks.api;

import com.github.javafaker.Faker;
import com.socks.api.entity.User;

public class TestData {

    public static User getUserForTest() {

        Faker faker = new Faker();

        return new User()
                .setUsername(faker.name().username())
                .setFirstName(faker.name().firstName())
                .setLastName(faker.name().lastName())
                .setEmail("user@test11.com")
                .setPassword("user11");
    }
}
